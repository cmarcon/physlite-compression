# physlite-compression


The starting reference for this code is [this repo](https://gitlab.cern.ch/amete/athena-sandbox).


The purpose of this code is to post-process PHYSLITE files to compress (high or low level) containers or individual variables.
The compression applied in this code is LOSSY, i.e. bits are truncated and lost.


The workflow is the following:

1) First, setup Athena with `asetup Athena,<Version Tag>`
2) Given a PHYSLITE file, a JSON dump of all its containers and variables is extracted by running the `GenerateJsonConfigFile.py` script (use the option `--help` to get relevant usage hints)
3) For each container or variable listed in the JSON file, it is possible to set the compression level: 0 -> no compression, >0 -> high compression, <0 -> low compression.
4) The `CompressFromJson.py` script can then be run with a command line similar to this: `python CompressFromJson.py --inputFiles ../DAOD_PHYSLITE.art.pool.root --outputFile myCompressedDAOD_PHYSLITE.pool.root --compressionConfig ../DAOD_PHYSLITE.art.pool.root_branches.json --highMantissaBits 7 --lowMantissaBits 15`
5) The last two options allow to change the number of bits retained for high and low compression levels, respectively.
5) This produces a new PHYSLITE file where only the containers/variables set to either high or low compression are modified and the rest is left untouched.

To run on the GRID with prun:

`prun --noBuild --exec "python CompressFromJson.py --inputFiles %IN --outputFile compressed.root --compressionConfig compression_config.json --highMantissaBits 7 --lowMantissaBits 15" --inDS "<INPUT DATASET ID>" --outDS "<OUTPUT DATASET ID>" --athenaTag Athena,<Version Tag> --outputs compressed.root`



